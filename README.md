# cache-http

Package cache-http provides a http.RoundTripper implementation that works as a mostly RFC 7234 compliant cache for http responses.

Fork from: https://github.com/gregjones/httpcache